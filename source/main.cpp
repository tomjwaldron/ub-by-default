// dead basic blinky for nucleo board, uses timer rather than cpu
// written in a very obtrusive but honest manor

int main()
{
  *reinterpret_cast<unsigned*> (0x4002'1014) = 0x0002'0014;  // enable gpio A
  *reinterpret_cast<unsigned*> (0x4002'101c) = 0x0000'0001;  // enable timer 2

  asm volatile ("dsb");   // memory barrier, wait for memory ops to complete

  *reinterpret_cast<unsigned*> (0x4800'0000) = 0xa800'0800;  // gpio a5 af mode
  *reinterpret_cast<unsigned*> (0x4800'0020) = 0x0010'0000;  // gpio a5 af1
  *reinterpret_cast<unsigned*> (0x4000'0028) = 0x0000'0f9f;  // timer2 prescaler set to 4000 (-1)
  *reinterpret_cast<unsigned*> (0x4000'002c) = 0x0000'03e7;  // timer2 reload value 1000 (-1)
  *reinterpret_cast<unsigned*> (0x4000'0034) = 0x0000'03e7;  // timer2 compare value 1000 (-1)
  *reinterpret_cast<unsigned*> (0x4000'0020) = 0x0000'0001;  // timer2 channel 1 output enable
  *reinterpret_cast<unsigned*> (0x4000'0018) = 0x0000'0038;  // timer2 toggle mode

  asm volatile ("dsb");   // memory barrier, wait for memory ops to complete

  *reinterpret_cast<unsigned*> (0x4000'0000) = 0x0000'0001;  // timer2 enable

  asm volatile ("wfi");   // 'wait for interrupt' (sleep)

  return 0;               // we never get here
}

// called from startup asm, before main()
extern "C" void SystemInit()
{
}
