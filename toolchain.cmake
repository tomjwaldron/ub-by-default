# baremetal arm
set (CMAKE_SYSTEM_NAME      Generic)
set (CMAKE_SYSTEM_PROCESSOR cortex-m4)

set (TC_PATH "")

set (CROSS_COMPILE arm-none-eabi-)

set (CMAKE_C_COMPILER   "${TC_PATH}${CROSS_COMPILE}gcc")
set (CMAKE_CXX_COMPILER "${TC_PATH}${CROSS_COMPILE}g++")
set (CMAKE_LINKER       "${TC_PATH}${CROSS_COMPILE}ld")
set (CMAKE_ASM_COMPILER "${TC_PATH}${CROSS_COMPILE}gcc")

set (CMAKE_OBJCOPY "${TC_PATH}${CROSS_COMPILE}objcopy"
     CACHE FILEPATH "toolchain objcopy command" FORCE)

set (CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# search for programs in the build host dirs
set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# search for libs and headers in the target dirs
set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mcpu=cortex-m4")
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mfloat-abi=hard")
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mfpu=fpv4-sp-d16")
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mtune=cortex-m4")

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mcpu=cortex-m4")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mfloat-abi=hard")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mfpu=fpv4-sp-d16")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mtune=cortex-m4")

set (CMAKE_ASM_FLAGS "${CMAKE_C_FLAGS} -x assembler-with-cpp")

# cache for use
set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "CFLAGS")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}" CACHE STRING "CXXFLAGS")
set (CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS}" CACHE STRING "ASMFLAGS")
