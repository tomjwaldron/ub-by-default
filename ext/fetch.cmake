cmake_minimum_required(VERSION 3.11)

include(FetchContent)

# fetch svd file
FetchContent_Declare(
  svd
  URL https://www.st.com/resource/en/svd/stm32f3_svd.zip
)
FetchContent_GetProperties(svd)
if(NOT svd_POPULATED)
  FetchContent_Populate(svd)
  file(COPY ${svd_SOURCE_DIR}/STM32F3_svd_V1.2/STM32F303.svd DESTINATION ext/svd)
endif()

# fetch startup asm file from st repo
file(
  DOWNLOAD
  https://raw.githubusercontent.com/STMicroelectronics/STM32CubeF3/master/Projects/STM32F303RE-Nucleo/Templates/SW4STM32/startup_stm32f303xe.s
  ext/st/startup_stm32f303xe.s
)

# fetch linker script from st repo
file(
  DOWNLOAD
  https://raw.githubusercontent.com/STMicroelectronics/STM32CubeF3/master/Projects/STM32F303RE-Nucleo/Templates/SW4STM32/STM32F303RE-Nucleo/STM32F303RETx_FLASH.ld
  ext/st/STM32F303RETx_FLASH.ld
)

# fetch openocd
if(CMAKE_HOST_SYSTEM_NAME STREQUAL "Darwin")
  set(OPENOCD_URL https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.10.0-13/xpack-openocd-0.10.0-13-darwin-x64.tgz)
  set(OPENOCD_SHA https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.10.0-13/xpack-openocd-0.10.0-13-darwin-x64.tgz.sha)
endif()
if(CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
  set(OPENOCD_URL https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.10.0-13/xpack-openocd-0.10.0-13-win32-x64.zip)
  set(OPENOCD_SHA https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.10.0-13/xpack-openocd-0.10.0-13-win32-x64.zip.sha)
endif()
if(CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux")
  set(OPENOCD_URL https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.10.0-13/xpack-openocd-0.10.0-13-linux-x64.tgz)
  set(OPENOCD_SHA https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.10.0-13/xpack-openocd-0.10.0-13-linux-x64.tgz.sha)
endif()
FetchContent_Declare(
  openocd
  URL           ${OPENOCD_URL}
  # URL_HASH      ${OPENOCD_SHA}
)
FetchContent_GetProperties(openocd)
if(NOT openocd_POPULATED)
  FetchContent_Populate(openocd)
  file(COPY ${openocd_SOURCE_DIR}/openocd/0.10.0-13/ DESTINATION ext/openocd)
endif()

# https://www.st.com/content/ccc/resource/technical/document/reference_manual/4a/19/6e/18/9d/92/43/32/DM00043574.pdf/files/DM00043574.pdf/jcr:content/translations/en.DM00043574.pdf
